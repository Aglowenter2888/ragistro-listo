<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/estilo.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">

    
    <h2  align="center">Repositorio de invenstigaciones y publicaciones </h2>
    <img id="hola"  src="{{asset('image/hola.png')}}">


        <main class="py-4">
            @yield('content')
        </main>
    </div>
    <br>
    <p align="center">
Universidad Tecnológica de Cancún
Organismo Público Descentralizado del Gobierno del Estado de Quintana Roo<br>
Carretera Cancún-Aeropuerto, Km. 11.5, S.M. 299, Mz. 5, Lt 1<br>
Cancún, Quintana Roo, C.P. 77565
Tel. 01 (998) 881 19 00
</p>
</body>
</html>
